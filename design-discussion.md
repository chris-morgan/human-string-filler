# A discussion of parts of the design and trade-offs in this library

## Terminology and semantics

- `Hello, {thing}!` is a template string.
- In that, `{thing}` is a template region.
- In that, `thing` is a key.
- `{{` is an escaped opening curly brace.
- `}}` is an escaped closing curly brace.

Beyond that, these are *suggestions* for template region semantics:

- In the `{post.date:short}` template region:
  - `post.date` is a path (the `date` field within the `post` object);
  - `short` is a format specifier, which we might here imagine to mean “use
    localised date formatting, short form”.

- In the `{post.date time hour=24}` and `{post.date:time,hour=24}`
  template regions (I’m not suggesting particular characters):
  - `post.date` is a path (the `date` field within the `post` object);
  - `time` is a boolean property, and `hour` a property with value `24`,
    which we might here imagine to mean “show the time, in 24-hour mode”.

## Explanation of escaping

It is necessary that literal `{` and `}` be able to be expressed, even if
they’re rare; there are three common techniques for achieving this:

1. **Reserve certain template region keys to signify the delimiters,** e.g.
   `{open_curly}` and `{close_curly}` to expand to `{` and `}`. Downsides: may
   clash with real template region keys when arbitrary user keys are supported;
   and it’s generally less memorable.
2. **Double the delimiter:** `{{` expands to `{` and `}}` to `}`. Seen in
   string delimiters in a few languages, so `"foo""bar"` means “foo"bar”.
   Downside: makes including the delimiters inside the key a difficult
   proposition (see below).
3. **Escape by prefixing with another selected character:** select an escape
   character which is also rare, most commonly a backslash due to its
   convention in programming languages. Problems with using backslash: if you
   need to represent Windows paths, literal backslashes aren’t so rare after
   all, and you need to escape them all; and if you need to encode the template
   string in a string literal in another language that uses the same escape
   character (e.g. a TOML config file), all backslashes must now be escaped
   again, e.g. string literals `"C:\\\\dir"` or `"\\{ ← literal curly"`.

(A fourth technique that can be employed is delimiting the replacement regions
with values that will not appear in the desired text, as with the ␜, ␝, ␞ and ␟
separator control characters in ASCII—and that more or less failed because it
was roughly machine-writable only, and so file types like CSV prospered due to
being human-writable, despite the acconpanying escaping woes. This language
requires that the delimiters be easy for the user to type, so this fourth
technique is deemed impossible—all values that are easy to type may occur in
regular text.)

One interesting consideration is whether you can express the delimiters inside
template regions. For simplicity, I’ve currently banned curlies inside template
regions, but there are things to consider if it becomes desirable to express it
later (e.g. in a format specifier). Here’s how each of the three techniques
previously mentioned handles it:

1. Solvable in two ways, both of which require that curlies be paired inside
   template regions:
   (a) Replace only `{open_curly}` and `{close_curly}` literally inside
       template regions; or
   (b) Perform recursive template resolution!

2. Well, should `{foo}}}` mean “template region with key ‘foo’ followed by
   literal ‘}’”, or “template region with key ‘foo}’”? If you wish both to be
   expressible, you need to provide some means of disambiguation. The most
   straightforward is to ban a certain character at the start and end of
   template regions, most probably HORIZONTAL SPACE (that is: declare “no
   leading or trailing whitespace”), and then repurpose that to behave
   essentially the same as `#` on Rust’s raw string literals, where you can
   write `r"…"`, `r#"…"`, `r##"…"##`, *&c.* With this, `{foo}}}` would mean
   “template region with key ‘foo’ followed by literal ‘}’”, and `{ foo} }`
   would mean “template region with key ‘foo}’”. (Note how escaping curlies
   inside template regions is not required. Decide for yourself whether that’s
   good or bad.)

3. Allow escapes inside the template region, e.g. `\{{\{\}}\}` would mean
   “literal ‘{’ followed by template region with key ‘{}’ followed by literal
   ‘}’”. Straightforward.

This then leads to three possibilities:
(a) full recursive template rendering;
(b) arbitrary keys, or *almost* arbitrary if leading and trailing whitespace is
    disallowed or ignored (depending on the approach); or
(c) no curlies in keys please.

I initially implemented the third technique (escape with backslash, and allow
escapes in template regions), but the downsides of using backslashes became too
severe for my liking, and I didn’t like any alternative characters on the
keyboard as much as just doubling the delimiter, so I went with the second
technique in the end. I don’t expect its downside to bite me, but we’ll see.

## Other considered syntaxes

I settled on using `{…}` for template regions. I also contemplated `$…`, but
ruled it for the combination of these reasons:

- it requires embedding UAX #31 identifier parsing which is heavier and less
  *definitely obvious* for real users than I’d like;

- for things like nested property access and format specifiers you’d need a
  variant with a closing delimiter anyway, such as `${…}`, so now you’d have
  *two* formats;

- all this makes it harder to explain all the features of the language to
  users.

So `$…` was eliminated as too complex and insufficient.

So it became more a toss-up between `$…$`, `%…%`, `${…}` and `{…}`.

- `$…$` was eliminated because it’s not using any paired delimiting characters,
  and because more programmery types would be more likely to expect `$…` than
  `$…$`.

- `%…%` has precedent in Windows’ environment variables, but again the lack of
  *paired* delimiting characters makes it not so great. Also it just feels
  uglier in the standard sort of case I have in mind. I’m picturing something
  like `%ALBUM%/%TRACK% - %TITLE%.mp3`. (It doesn’t *have* to be uppercase, but
  I find that using percent makes me think uppercase.)

- Given my intended purpose (*small* strings rather than big multi-line
  strings), the `$` in `${…}` didn’t feel necessary: consider for example
  `{album}/{track} - {title}.mp3`, which feels about right, compared with
  `${album}/${track} - ${title}.mp3`, which *works*, but feels… I dunno,
  programmery rather than usery.

So `{…}` won.

Taken with the `{{` and `}}` escaping, this makes it match Rust’s format
strings; this is purely coincidental.

I’m pretty sure I considered `[…]` at some point too, but decided I didn’t like
it as much as `{…}`.
